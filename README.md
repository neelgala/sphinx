# sphinx

This image contains the latest sphinx with python3.7+

Example to use registry in your .gitlab-ci.yml:


```yaml
image: registry.gitlab.com/incoresemi/dockers/sphinx:master
pdf_gen:
  script:
    - cd docs/
    - pip install -r requirements.txt
    - make latexpdf
  only:
    - master

```

